# [Qwirk Client](https://gitlab.com/ThibaultJeanLemaire/3PJT-QwirkClient)

Qwirk is a school project to build an audio/video and chat communication service having the same kind of features as Skype or Slack.

This project is the Java XMPP Client based upon the open-source multi-protocol client [Jitsi][jitsi github].

## Helpful Resources
- [Jitsi official website](https://jitsi.org)
- [Jitsi source repository][jitsi github]

[jitsi github]: https://github.com/jitsi/jitsi